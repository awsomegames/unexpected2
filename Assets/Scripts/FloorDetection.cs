using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDetection : MonoBehaviour
{
    #region Reference Variables
    [Header("References")]
    [SerializeField] private PlayerMovement playerMovement;
    #endregion

    #region Variables
    [Header("Variables")]
    [Tooltip("Time in seconds within player can still jump after leaving floor.")]
    [SerializeField] float tolerance;
    #endregion

    private bool isOnFloor = false;
    public bool IsOnFloor { get => isOnFloor; }

    #region Coroutine Variables
    private IEnumerator waitToBlockJumpCoroutine => WaitToBlockJumpCoroutine(tolerance);
    private Coroutine coroutine = null;
    #endregion

    #region Unity Life Cycle
    private void Start()
    {
        playerMovement.OnJumpStarted += delegate () {
            if (coroutine != null)
            {
                isOnFloor = false;
                StopCoroutine(coroutine);
            }
        };
    }
    #endregion

    #region Collision Callbacks
    private void OnTriggerEnter2D(Collider2D collider)
    {
        FloorProperties floorProperties = collider.GetComponent<FloorProperties>();
        if(floorProperties != null) {
            Debug.Log("Touches floor!");
            if(coroutine != null) {
                StopCoroutine(coroutine);
            }
            isOnFloor = floorProperties.canBeSteppedOn;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        FloorProperties floorProperties = collider.GetComponent<FloorProperties>();
        if(floorProperties != null) {
            Debug.Log("Leaves floor!");
            if(isOnFloor) {
                coroutine = StartCoroutine(WaitToBlockJumpCoroutine(tolerance));
            }
        }
    }
    #endregion

    private IEnumerator WaitToBlockJumpCoroutine(float seconds) {
        yield return new WaitForSeconds(seconds);
        if(isOnFloor) {
            Debug.Log("Extra time to jump ended!");
            isOnFloor = false;
        }
        yield return null;
    }
}
