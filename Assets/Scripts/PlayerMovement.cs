using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    #region Reference Variables
    [Header("References")]
    [SerializeField] private Rigidbody2D rbody = null;
    [SerializeField] private FloorDetection floorDetection = null;
    #endregion

    #region Movement Variables
    [Header("Movement Variables")]
    [SerializeField] private float moveSpeed;
    [Tooltip("The vertical speed that will be kept as long as you keep pressed the jump button.")]
    [SerializeField] private float jumpSpeed;
    [Tooltip("Time in seconds before automatically begin falling after jumping.")]
    [Range(0, 0.5f)]
    [SerializeField] private float maxJumpTime;
    #endregion

    #region Private Varibales
    private float currentHorizontalSpeed = 0;
    private bool isJumping = false;
    #endregion

    #region Coroutine variables
    private IEnumerator stopJumpingTimerCoroutine => StopJumpingTimerCoroutine(maxJumpTime);
    private Coroutine coroutine = null;
    #endregion

    #region Events
    public event Action OnJumpStarted;
    #endregion

    #region Context Menu Options
    [ContextMenu("Recommended Values")]
    private void _RecommendedValues() {
        moveSpeed = 8f;
        jumpSpeed = 12f;
        maxJumpTime = 0.1f;
    }
    #endregion

    #region Unity Life Cycle
    private void Update()
    {
        rbody.velocity = new Vector2(currentHorizontalSpeed, rbody.velocity.y);
        if(isJumping) {
            rbody.velocity = new Vector2(rbody.velocity.x, jumpSpeed);
        }
    }
    #endregion

    #region Input Listeners
    private void OnJump(InputValue inputValue) {
        float value = inputValue.Get<float>();

        if(value == 1) {
            if(floorDetection.IsOnFloor) {
                StartJumping();
            }
        }
        else {
            StopJumpingForced();
            if(coroutine != null) {
                StopCoroutine(coroutine);
            }
        }
    }

    private void OnMove(InputValue inputValue) {
        float moveValue = inputValue.Get<float>();
        currentHorizontalSpeed = moveValue * moveSpeed;
        
        if(moveValue != 0) {
            transform.localScale = new Vector3(
                Mathf.Sign(moveValue),
                transform.localScale.y,
                transform.localScale.z
            );
        }
    }
    #endregion

    #region Jumping Methods 
    private void StartJumping() {
        isJumping = true;
        coroutine = StartCoroutine(stopJumpingTimerCoroutine);
        OnJumpStarted?.Invoke();
    }

    private void StopJumpingNatural() {
        if(isJumping) {
            isJumping = false;
        }
    }

    private void StopJumpingForced() {
        if(isJumping) {
            isJumping = false;
            rbody.velocity = new Vector2(rbody.velocity.x, 0);
        }
        else if(rbody.velocity.y >= 0) {
            rbody.velocity = new Vector2(rbody.velocity.x, 0);
        }
    }

    private IEnumerator StopJumpingTimerCoroutine(float seconds) {
        yield return new WaitForSeconds(seconds);
        if(isJumping) { StopJumpingNatural(); }
        yield return null;
    }
    #endregion
}
