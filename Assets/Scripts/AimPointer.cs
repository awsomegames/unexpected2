using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AimPointer : MonoBehaviour
{
    #region Reference Variables
    [Header("References")]
    [SerializeField] private Camera mainCamera;
    #endregion

    #region Input Listeners
    private void OnPoint(InputValue inputValue) {
        Vector2 value = inputValue.Get<Vector2>();
        Vector3 newPos = new Vector3(
            value.x,
            value.y,
            transform.position.z
        );
        Vector3 newWorldPos = mainCamera.ScreenToWorldPoint(newPos);
        newWorldPos = new Vector3(
            newWorldPos.x,
            newWorldPos.y,
            0
        );
        transform.position = newWorldPos;
    }
    #endregion
}
