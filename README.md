# Description
A roughlike hd platformer with unexpected elements.

Started as a  Game Jam simulation under the theme "Unexpected".

# Team
Raúl Alex Chávez (Programmer)

Ashley Villaseñor (Pixelart)

Arturo Santos (Pixelart)

Valeria López (Pixelart)
